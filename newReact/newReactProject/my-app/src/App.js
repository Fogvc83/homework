import React from 'react';

import Hello from "./Components/Hello";
import ToggleTheme from "./Components/ToggleTheme";

const App =() =>
    <div>
    <Hello
        user={
        {
            firstName: "Valeriy",
            lastName: "PAHOMOV",
        }}
        showLastName={true}
    />
    <ToggleTheme/>
    </div>;

export default App;