import React, {Component} from 'react';

// class Hello extends Component{
//     render() {
//         console.log('props:', this.props);
//
//         const {
//             user,
//             showLastName,
//         }=this.props
//
//         return (
//             <div>
//                 <h1> Hi class  {user.firstName} {showLastName && user.lastName}!</h1>
//             </div>
//         );
//     }
// }


// тоже самое в функциональных компонентах
// function Hello(props){
//     const {
//         user,
//         showLastName,
//     }=props
//     return(
//         <div>
//             <h1> Hi class  {user.firstName} {showLastName && user.lastName}!</h1>
//         </div>
//     )
// }

// тоже самое в стрелочной функции

const Hello = ({user,showLastName}) =>
    (<div>
        <h1> Hi class  {user.firstName} {showLastName && user.lastName}!</h1>
    </div>)

export default Hello;