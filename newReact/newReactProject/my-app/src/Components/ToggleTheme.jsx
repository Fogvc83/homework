import React, { Component } from 'react';
import "../style/theme.css";

class ToggleTheme extends Component{

    constructor() {
        super();
        this.state = {
            isDark: true
        }
        this.handleToggleClick=this.handleToggleClick.bind(this);
    }

handleToggleClick() {
        this.setState({
            isDark: !this.state.isDark
        })
}


    render() {

        // задаём стиль dark и стиль для кнопки
        const themeClassName = this.state.isDark ? 'dark': '';
        const styles ={
            height: 60,
            weight: 150,
        }

        return <div>
            <span>{this.state.isDark ? "Dark" : 'Light'}</span>;

            {/*//  className зарезервированное слово для класса*/}
            <button className={themeClassName}
                    style={styles}
                    onClick={this.handleToggleClick}> Toggle Theme</button>

        </div>;
    }
}

export default ToggleTheme;