import React from "react";
import ReactDOM from "react-dom";

import App from "./App";


// npm start - для запуска

// вставляем заголовок
// ReactDOM.render(<h1> Hello </h1>, document.getElementById("root"));


// вставляем функцию - функциональній компонент
// const Hello = () => (
// <div>
//     <h1> Hi! </h1>
// </div>
// );


// вставляем класс. пишем название класса с большой буквы
// переносим компонент в отдельный файл Hello
// class Hello extends React.Component{
//     render() {
//         return (<div>
//             <h1> Hi class! </h1>
//         </div>);
//     }
// }

// переносим компонент в отдельный файл ToggleTheme
// class ToggleTheme extends React.Component{
//     render() {
//         return null;
//     }
// }

// переносим компонент в отдельный файл App
// const App =() => <div>
//     <Hello />
//     <ToggleTheme />
// </div>;
//

ReactDOM.render(
        <div> <App /> </div>,
    document.getElementById("root")
);