import React from "react";


class Post extends React.Component{
    render() {
        return(
            <p>
                {this.props.text};
            </p>
        )
    }

}

export default Post;