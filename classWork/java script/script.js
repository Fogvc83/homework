// простые типы данных
// number, string, null, undefined, boolean
//
// составные (Linked)
// object
//
// symbol, bigint

// let num = 12;
// let num1 = 22;
// let str = `Hello
// World`
//
// let str2 = 'Hello'
// let str3 = 'World'

// конкотонация строк
// let sumOfStr = str2 + " " + str3 +'!';
// console.log(tempLateStr);
//
// let tempLateStr = `str2  str3 ${num!`;
// // в строку вставляем переменные(шаблонная строка)
// let tempLateStr = `${str2}  ${str3} ${num + num1}!`;

// console.log(tempLateStr);
//
// console.log(massage);
// будет выведено "undefined" так как переменная не имеет ни какого значения только для var так как она задана ниже
// если const или let то будет ошибка



// var переменная
// var massage = 'hello';
// console.log(massage);
//
// massage = 'good bye';
// console.log(massage);
//
// // задаём переменну // инициализация переменной
// let test;
// console.log('test');

// let test; задавать после вызова нельзя в отличии от var

// const massage2 = 'hello';
// // massage = 'welcom'; будет ошибка так как константу нельзя изменять
// // console.log(massage2);
//
// // объект
// const student = {
//     name: 'John',
//     programingLenguage: 'js',
// }
//
// // typeof - тип переменной, константы
//
// console.log(student);
// console.log(typeof student);
//
// const year = 1991;
// powerOff = true;
// powerOn = false;
// console.log(powerOff, typeof powerOff);
// console.log(powerOn, typeof powerOn);
// console.log(year, typeof year);
//
// // тип NaN number
// console.log(typeof NaN);
//
// // тип null object
// console.log(typeof null);
//
//
// const a = 2;
// const b = 3;
// const c = a + b;
//
// console.log(c);
// console.log(c**5);
//
// // оператор остатка от деления вывидет 2  0  1
//  console.log(7 % 5);
// console.log(4 % 2);
// console.log(10 % 3);
//
// const quantity = 20;
// const price = 50;
// console.log(total = price*quantity);
//
//
// const studentCountInGroup1 = 200;
// const studentCountInGroup2 = 120;
// const studentCountInGroup3 = 145;
// const studentCountInGroup4 = 45;
//  const studyPrice = 450;
//
//  const totalAmount = (studentCountInGroup1 + studentCountInGroup2 +studentCountInGroup3 + studentCountInGroup4) * studyPrice;
//
//  console.log("totalAmount = ", totalAmount);
//
//  const sideA = 7;
//  const sideB = 20;
//
// const sideC = (sideA**2 + sideB**2)**0.5;
//  console.log(sideC);
//
//
//  const diameter = 10;
//  const height = 33.3;
//  const p = 3.14;
//
//  const volumeCylinder = p * (diameter/2)**2 * height;
// console.log(volumeCylinder);
//
// console.log('Log');
// console.info('info');
// console.warn('warn');
// console.error('error');
// console.table(['apples', 'oranges', 'bananas']);
//
//

// const massageAlert = "Alert с сообщением блокирует выполнение потока до подтверждения"
// console.log('Start');
// alert(massageAlert);
// console.log('Finish');


// форма ввода

// const massagePrompt = prompt('Введіть кількість', 0);
//
// console.log(massagePrompt);
// console.log(typeof massagePrompt);



// форма подтверждения
// const isFormSent = confirm('отправить форму');
// if (isFormSent) {
//     console.log(' Форма успешно отправлена');
// }
// else {
//     console.log(' Форма не была отправлена');
// }

let red = +prompt('Задайте красный цвет', 0);
let green = +prompt('Задайте зелёный цвет', 0);
let blue = +prompt('Задайте синий цвет', 0);

// + для перевода в числа из строк
console.log(red.toString(16), green.toString(8), blue.toString(2));


const num = 12.365;
num.toFixed(2); //округление до 12,37

num.toFixed(5); //округление до 12,36500

isFinite(12); //конечное или бесконечное число ответ true или false


parseInt('12.543'); // переводит в целое число 12
parseInt('test'); // ответ NaN не число
parseFloat('12.4jij') // ответ 12.4 буквы откинуты


const firstNumber = +prompt('Ввидите первое число',0);
const secondNumber = +prompt('Ввидите второе',0);
const mathematicalOperetion = prompt('Введите математическую опперацию : +, -, *, /', '+');

function resultCalc (firstNumber, secondNumber, mathematicalOperetion) {
    switch (mathematicalOperetion) {
        case '+' :
            return firstNumber + secondNumber;
        case '-' :
            return firstNumber - secondNumber;
        case '*' :
            return firstNumber * secondNumber;
        case '/' :
            return firstNumber / secondNumber;
    }
}