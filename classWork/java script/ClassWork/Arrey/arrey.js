
//нулевой масив с именем list
const list = [];
console.log(list);

//масив с именем list5 в котором один элемент 6 и длинной1
const list5 = [6];
console.log(list5.length);
console.log(list5);

//нулевой масив с именем list1 задан как функция
const list1 = new Array();
console.log(list1);

//масив с именем list6 задан как функция c 10 элементами
const list6 = new Array(10);
console.log(list6);

//масив с именем list2
const list2 = [0, 1, 2, 3];
console.log(list2);

//масив с именем list3 задан как функция и емеет 5 элементов
const list3 = new Array(5);
console.log(list3);

//масив с именем list7 задан элемент 2 равный 10 и 6 равный пть, остальные undefined
const list7 = [];
list7[1] = 8;
list7[5] = 5;
console.log(list7);
console.log(list7[3]);



const day=[
    "sanday",
    "manday",
    "tysday",
    "wensday",
    "thesday",
    "frayday",
    "seterday",
];

//выводим второй елемент массива. нмерация с нуля
console.log(day[1]);

//выводим длинну массива
console.log(day.length);

//выводим последний элемент массива. нмерация с нуля
console.log(day[day.length-1]);

//именение елемента массива. нмерация с нуля
day[1] = "apple";
console.log(day);

//удаление елемента массива. нумерация с нуля
delete day[4];
console.log(day);

//удаление елементов массива всех после пятого. нумерация с нуля
// массив стал длинной в пять элементов
day.length = 5;
console.log(day);

//добавление одного масива к другому
let arr1 = ['apple', 'lemon', "orrange"];
let arr2 = ['mango', 'pinapple'];
let arr3 = arr1.concat(arr2);
console.log(arr3);

//добавляем последний елемент масива со значением 8
arr3.push(8);
console.log(arr3);

// метод push возвращает длинну масива
const res = arr3.push(8);
console.log(arr3);
console.log(res);

// метод push возвращает длинну масива
const res0 = arr3.push(8, 7, 6);
console.log(arr3);
console.log(res0);

// метод pop вырезаетпоследний элемент масива
arr3.pop();
console.log(arr3);

// метод pop вырезает и передаёт значение последнего элемента в перемнную
const item = arr3.pop();
console.log(arr3);
console.log(item);

//добаление нового элемента в начало масива. индексы всех последующих елементов переписываютьс
const newElement = arr3.unshift(1);


//удаление элемента с начала масива(первого єлемента). индексы всех последующих елементов переписываютьс
//функция может вовращать выреззанный элемент
const newElement2 = arr3.shift();

//вовращает новую длинну масива
console.log(newElement);

//вырезает элемент и содаёт масив, если нет элемента раобьет по символам
let arr9 = 'apple[lemon[orrange[mango[pinapple';
console.log(arr9.split("["));