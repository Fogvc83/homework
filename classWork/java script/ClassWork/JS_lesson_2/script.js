let a = 1;
let b = 1;
let c = 1;


let A = B = C = 2;

console.log(a,b,c);
console.log(A,B,C);


let d = 4;
let e = 10;

let f;


f = d;
d = e;
e = f;


// меняем местами значения двух переменных
g = 12;
h = 15;

g = g + h;

h = g - h;

g = g - h;


console.log(g, h);

let billion = 1e9;
console.log(billion, typeof billion);
// 1000000000 'number'

let billion2 = 1e-5;
console.log(billion2, typeof billion2);
//  0.00001 'number'

const a2 = 0b010101
const hex = 0xff;
const b8 = 0o345;
console.log(a2, hex, b8);
// 21 255 229   двуичная, шестнадцатиричная, восьмиричная системы исчисления

// перевод в строку переменной hex в десятиричной системе исчисления
hex10 = hex.toString(10)
hex.toString()
console.log(hex10);

// перевод в строку переменной hex в шестнадцатиричной системе исчисления
hex16 = hex.toString(16)
console.log(hex16);

// перевод в строку переменной hex в восмиричной системе исчисления
hex8 = hex.toString(8)
console.log(hex8);

// перевод в строку переменной hex в двуичной системе исчисления
hex2 =hex.toString(2)
console.log(hex2);

// перевод в тип строка
billion = billion.toString(2);
console.log(billion, typeof billion);



// let Red = +prompt('Введите красный цвет в формате RGB', 0);
// let Grean = +prompt('Введите красный цвет в формате RGB', 0);
// let Blue = +prompt('Введите красный цвет в формате RGB', 0);
//
// console.log(Red, Grean, Blue);
//
// console.log(Red.toString(16), Grean.toString(16), Blue.toString(16));



const num = 3.0678;
num1 = num.toFixed(1);  // округление до 1 цыфры после зяпятой возвращает строку
console.log(num1);
num2 = num.toFixed(2);  // округление до 2 цыфр после зяпятой возвращает строку
console.log(num2);

num3 = num.toFixed(6);  // округление до 3 цыфр после зяпятой возвращает строку добавляет недостоющие нули 3.067800
console.log(num3);

console.log(0.1+0.2);   // oтвет  0.30000000000000004
console.log((0.1+0.2).toFixed(2)); // округление до 2 цыфр после зяпятой возвращает строку ответ 0.30

const prcnt = 26.75;
// let sum = +prompt("ведите сумму кредита", 10000);
// let years = +prompt(" Введите количество лет для кредита", 1);
//
// let paid = (sum/100)*prcnt;
//
// let overpayment = paid * years;
//
// let totalSum = sum + overpayment;
//
// console.log('Переплата = ', overpayment, 'Вся сумма = ', totalSum);

// конечное или не конечное число
console.log(isFinite(prcnt));  // true
console.log(isFinite(Infinity))  // false ( Infinity - бесконечность)
console.log(isFinite(-Infinity)) // false ( - Infinity - минус бесконечность)

console.log(isFinite(NaN)) // false
console.log(isFinite('nfinity')) // false
console.log(isFinite('12')) // true

// роверка на NaN
console.log(isNaN(prcnt));  // false это  цифра
console.log(isNaN(Infinity))  // false ( Infinity - бесконечность) это цифра
console.log(isNaN(null)) // false это цифра

console.log(isNaN(NaN)) // true это не цифра
console.log(isNaN('nfinity')) // true это не цифра
console.log(isNaN('12')) // false это цифра переводит в число
console.log(isNaN(true)) //  false  это цифра переводит в число


// перевод в целое число
console.log(parseInt('12.4'));  // переводит в целое число 12
console.log(parseInt('nfinit'));  // NaN
console.log(parseInt('  sd112nfinit  '));  // NaN
console.log(parseInt('  112nfinit34  '));  // 112

console.log(parseFloat('  112nfinit34  '));  // 112
console.log(parseFloat('  1120.34  '));  // 112.34 число с плавающей запятой


 // у меня не заработало
console.log(parseFloat('  112.34  ', 10));  // 112.34 число с плавающей запятой 10 ричнея система исчисления
console.log(parseFloat('  ff  ', 16));  // 112.34 число с плавающей запятой 16 ричнея система исчисления
console.log(parseFloat('  112.34  ', 8));  // 112.34 число с плавающей запятой 8 ричнея система исчисления
console.log(parseFloat('  111.11 ', 2));  // 112.34 число с плавающей запят 2 ричнея система исчисления

console.log(parseInt('  11111  ', 10));  // = 11111 в 10 ричнея система исчисления
console.log(parseInt('  11111 ', 16));  //  = 69905 в 16 ричнея система исчисления
console.log(parseInt('  11111  ', 8));  // = 4681 в 8 ричнея система исчисления
console.log(parseInt('  11111 ', 2));  //  = 31  в 2 ричнея система исчисления


// математические операции Math
degree = Math.pow(5,2);  // возведение в степень
console.log(degree);

least = Math.min(12,35,78);  // выведет наименьше
console.log(least);

let СМ = 2345
maximum = Math.max(256, -34, 65, СМ) // выведет максимальное значение (можно вставлять переменные
console.log(maximum);

 // строгое сравнение
console.log( 1 === '1'); // не равно false нету приведения типов

// не строгое сравнение
console.log( 1 == '1'); //  равно true есть приведение типов

// округление к ближайшему большему меньшему
console.log (Math.floor(2.83));  // ответ 2
console.log (Math.floor(-2.83));  // ответ -3

// округление к ближайшему большему
console.log (Math.ceil(2.3));  // ответ 3
console.log (Math.ceil(-2.3));  // ответ -2

// округление по правилам
console.log (Math.round(2.3));  // ответ 2
console.log (Math.round(-2.3));  // ответ -2

// откидывает дробную часть
console.log (Math.trunc(2.8));  // ответ 2
console.log (Math.trunc(-2.6));  // ответ -2

// псевдослучайное число
console.log (Math.random());
console.log (Math.random());

// сгенерировать случайное число от 0 до 100

let rundomNumber;
rundomNumber = Math.random() * 100;
rundomNumber = Math.trunc(rundomNumber);
console.log (rundomNumber);


// let rundomNumber1 = +prompt("Введите число число от 0 до 100", 0);
//
// rundomNumber2 = Math.trunc(Math.random() * 100);
// console.log (Math.min(rundomNumber1, rundomNumber2));
// console.log (Math.max(rundomNumber1, rundomNumber2));


const exampl = 123;
const exampl2 = 567;
console.log(String(exampl));  // перевод в строку

console.log("exampl" / "exampl2"); // NaN
console.log("exampl" * "exampl2"); // NaN
console.log("exampl" - "exampl2"); // NaN
console.log("exampl" + "exampl2"); //examplexampl2

console.log(exampl + exampl2); // 690
console.log("exampl" + "|" + "exampl2");  //exampl|exampl2


console.log(Number(exampl));  // 123
console.log(Number(NaN)); // NaN
console.log(Number(null)); // 0
console.log(Number(undefined)); // NaN
console.log(Number(true)); // 1
console.log(Number(false)); // 0


console.log(Boolean(exampl)); //true
console.log(Boolean(NaN)); // false
console.log(Boolean(null)); // false
console.log(Boolean(undefined)); // false
console.log(Boolean(true)); //true
console.log(Boolean(false)); // false

console.log(Boolean("Text")); //true
console.log(Boolean("")); // false
console.log(Boolean(" ")); //true не пусто есть пробел

console.log(!!(true)); //true  !! - тоже что и Boolean приведение в логику

console.log(!!(exampl)); //true  !! - тоже что и Boolean приведение в логику
let exampl3;
console.log(!!(exampl3)); // false  !! - тоже что и Boolean приведение в логическую переменную (у нас пустая переменная)

// let firstСhange = prompt("Введите первую переменную", 0);
// let theChange = confirm("Увеличить заданную переменную на 1");
//
// let result = Number(firstСhange) + Number(theChange);
// console.log(" Результат = ", result);


// let firstСhange2 = prompt("Введите первую переменную", 0);
// let firstСhange3 = prompt("Введите вторую переменную", 0);
// let theChange2 = confirm("Увеличить заданную переменную на " + firstСhange3 + "?");
//
// let result2 = Number(firstСhange2) + Number(firstСhange3)*Number(theChange2);
// console.log(" Результат = ", result2);

let nameUser = prompt("Введите Имя");
let yearOfBirth = prompt("Введите год рождения", 2000);
let rundomNumber2 = Math.trunc(Math.random() * 100);
let theChangeYearOfBirth = confirm("Скрыть дату рождения");
let loginUser = nameUser + (Number(yearOfBirth) + Number(rundomNumber2) * Number(theChangeYearOfBirth));
console.log (" Логин ", loginUser);

