function createNewUser() {
    let newUser = {
        firstName: prompt('Enter First Name', 'Name'),
        lastName: prompt('Enter Last Name', 'Surname'),
        birthday: prompt('Enter birth date', 1990),
        getLogin: function(){
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function(){
            let currentDate  = new Date();
            console.log('Date', currentDate);
            let birthDate = new Date(this.birthday);
            console.log('Date', birthDate);
            if(currentDate.getMonth() > birthDate.getMonth()) {
                return  currentDate.getFullYear() - birthDate.getFullYear();
            }
            else {
                if(currentDate.getDay() > birthDate.getDay()) {
                    return currentDate.getFullYear() - birthDate.getFullYear();
                } else {
                    return  currentDate.getFullYear() - birthDate.getFullYear() - 1;
                }
            }
        },
        getPassword: function(){
            let birthDate = new Date(this.birthday);
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + birthDate.getFullYear();
        },
    };
    return newUser;
}

user01 = createNewUser();

console.log(user01);
console.log("User Login: " + user01.getLogin());
console.log("User Age: " + user01.getAge());
console.log("User Pass: " + user01.getPassword());