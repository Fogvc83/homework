
let project_folder = '#dist';
let source_folder = '#src';

let path = {  // переменная path с объектами в которых пути к папкам
    build:{ // пути  вывода к папке проекта
        html: project_folder + "/",
        css: project_folder + "/css/",
        js: project_folder + "/js/",
        img: project_folder + "/img/",
        fonts: project_folder + "/founts/",
    },

    src:{ // пути  вывода к исходным папкам проэкта
        html: [source_folder + "/*.html", "!" + source_folder + "/_*.html"], // делаем исключение для файлов с нижним подчёркиванием
        css: source_folder + "/scss/style.scss",
        js: source_folder + "/js/script.js",
        img: source_folder + "/img/**/*.{ipg, png, svg, gif, ico, webp}",
        fonts: source_folder + "/founts/*.ttf",
    },

    watch:{ // пути  вывода к файлам, которые постоянно слушаем (проверяем на обновления)
        html: source_folder + "/**/*.html",
        css: source_folder + "/scss/**/*.scss",
        js: source_folder + "/js/**/*.js",
        img: source_folder + "/img/**/*.{ipg, png, svg, gif, ico, webp}",

    },

    clean: './' + project_folder + '/',

}

// переменная сценария, которой присвоен сам gulp
let { src, dest} = require('gulp'),
    gulp = require('gulp'),
    browsersync = require('browser-sync').create(), // переменная для сценария browser-sync
    fileinclude = require('gulp-file-include'); // // переменная для сценария gulp-file-include - который собирает несколько файлов в один

function browserSync (params){ // функция для обновления страницы
    browsersync.init({
        server:{  // создаём сервер
            baseDir: './' + project_folder + '/'    // базовая папка для загрузки
        },
        port:3000,    // порт по которому открівается браузер
        notify: false  // отключается информационная табличка про обновления браузера
    })
}

function html(params){   // функция для работы с HTML файлами
    return src(path.src.html)  //  путь к переменной html в path.src. где html: source_folder + "/*.html",
        // .pipe это функция в которой мы пишем команды для gulp
        .pipe(fileinclude()) // вызываем переменную для сбора файлов в один
        .pipe(dest(path.build.html))  // папка с результатом проэкта после обработки gulp
        .pipe(browsersync.stream())    // обновление страницы
}


// функция для отслеживания изменений в файлах
function watchFiles(params){
    gulp.watch([path.watch.html], html) // указываем путь [path.watch.html] к файлу который отслеживаем и функцию которая этот файл обрабатывает html
}



let build = gulp.series(html); // функции которые будет выполнять gulp
let watch = gulp.parallel(build, watchFiles, browserSync); // сценарий выполнения


// при запуска gulp выполняются эти команды
exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;
